﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //offset the camera behind the player
        transform.position = Player.transform.position + new Vector3(0, 0.5f, -0.5f);
        transform.rotation =  Player.transform.rotation;
    }
}
